---
title: Catch in Promises... Why? How?
---
Promises can handle multiple asynchronous operations easily and provide better error handling than callbacks. They are the ideal choice for handling multiple callbacks at the same time, thus saves us from falling into what is called a callback hell situation. Promises do provide a better code readibility specially in case where there are multiple aschronous functions lined up one after another.

### Callback

``` bash
first(para, function(err,data){
    second(data, function(err, result){
        third(result, function(err){
            final();
        });
    });
});
```
### Promise

``` bash
first(para)
  .then((data) => second(data))
  .then((result) => third(result) )
  .then(() => final())
```
## Catch

### Error handling in Promises

So far we learnt Promises are better when working with multiple async operations. Another aspect in which Promises are better is error handling.Promise chains we saw earlier, are great at error handling. When a promise rejects, the control jumps to the closest rejection handler. That’s very convenient in practice.

For instance, in the code below the invalid URL to fetch (no such site) and .catch handles the error:

``` bash
fetch('https://no-such-server.') // rejects
  .then(response => response.json())
  .catch(err => alert(err)) // TypeError: failed to fetch
```

### Catch in Promise Chain

.catch works like the try-catch statement, which means you only need one catch at the end:

``` bash
first(para)
  .then((data) => second(data))
  .then((result) => third(result) )
  .then(() => final())
  .catch((err) => console.error(err))
```

### Unhandled error rejection

UnhandledPromiseRejectionWarning originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). A rejected promise is like an exception that bubbles up towards the application entry point and causes the root error handler to produce that output. It usually happens in async await functions, and can be easily handled with .catch block.

What happens if we did not add .catch at the end of is the script dies with a message in the console. A similar thing that happens with unhandled try-catch.

The unhandledrejection event is sent to the global scope of a script when a JavaScript Promise that has no rejection handler is rejected.

``` bash
new Promise(function() {
  errorFunction(); // Error here
})
  .then(() => {
    // successful promise handlers, one or more
  }); // without .catch at the end!
```

### Summary

* All kinds of error can be handled using .catch in promises, that could be reject(), or an error thrown in a handler.
* If you don’t have only one single .catch block in your Promise chain, you’re doing it wrong. One single .catch is enough to handle error in a single promise or in promise chain.
* If you know there is no way to handle the error then, you can opt out .catch


